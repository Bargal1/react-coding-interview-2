**Welcome in Bitnoise coding challenge #2**

**1. Where to start?**
- make sure that you have git and node 12
- fork this repository:
https://bitbucket.org/btndev/react-coding-interview-2/src/master/

- run from your CLI `yarn install`
- run from your CLI `yarn dev`
- have fun and share your skills
- commit your changes (show your progress in small chungs of changes)
- share your results with us
- wait for a feedback session
---

**2. Problem**

- Build a simple spreadsheet-like app from scratch.
- Use simple 10x10
- Make it

Steps to follow:

1. Render simple matrix, but try to figure out proper compoenents names ie. Cell, Spreadsheet etc.

_expected result:_

![Matrix](public/step_1_matrix.png)

2. Add labels for rows and columns ie. 

_expected result:_

![Matrix](public/step_2_labels.png)

3. Implement the state object which will keep the state of your spreadsheet, so the UI will visualisize the data state. To show us that it's working add simple onClick event to each cell of your spreadsheet. Click event should increase value of clicked cell by 1.

4. Make cell of your spreadsheet editable. On click replace value with input field and allow user to change the value of cell. Keep this value in spreadsheet data state. Close the input field on "enter" key event or when user will click outside of the input/cell.

_expected result:_

![Matrix](public/step_4_editable.png)

5. Add simple evaluation of values. When state changed, go through all cells and evaluate simple math equations like:
- when user put `1+2` into cell in edit mode, he should see `3` in read mode
- when user put `2/3` into cell in edit mode, he should see `0,66` in read mode
- when user put `Math.sin(30)` he should see `0,5`
etc.
Keep in mind to show `#error` when evaluation failed (ie. divide by 0)

6. Bonus step: create step referencing. 
- When user enter `A1 + A2` and cell `A1=1` and `A2=2` he should get `3` as result in view mode. 

7. Bonus question: think about possible optimizations to handle bigger spreadsheet data object and calculations.