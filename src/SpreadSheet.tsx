import React from 'react';
import Cell from './Cell';

let rows = [];
let label = [];
let labels = ["A", "B", 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
label.push(<th></th>);
for (let k = 0; k < labels.length; k++){
    label.push(<th>{labels[k]}</th>)
}
rows.push(<tr>{label}</tr>);
for (let i = 0; i < 10; i++) {
    const cells = [];
    for (let j = 0; j < 10; j++) {
        if(j == 0){
            cells.push(<th>{i}</th>)
        }
        cells.push(<Cell x={labels[j]} y={i}/>);
    }
    rows.push(<tr>{cells}</tr>);
}

const SpreadSheet  = () =>{
    return(
        <table className='spreadSheet'><tbody>{rows}</tbody></table>
    )
}

export default SpreadSheet;