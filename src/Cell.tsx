import React, {Component} from 'react';

class Cell extends Component{

    state = {
        cellValue: 0
    }

    constructor(props){
        super(props);
        this.myRef = React.createRef();
    }

    onCellSubmit = (oEvent) => {
        var id = this.myRef.current.id;
        var value;
        if (oEvent._reactName == 'onSubmit'){
            oEvent.preventDefault();
            value = oEvent.target[0].value;
        }
        else{
            value = oEvent.target.value;
        }
        var potentialCells = [...value.matchAll(/[0-9][A-Z]/ig)];
        var swapped = false;
        if (potentialCells.length > 0){
            for(var i = 0; i < potentialCells.length; i++){
                 for (var j = 0; j < window.$submited.length; j++){
                     if(potentialCells[i][0].toUpperCase() == window.$submited[j].id.toUpperCase()){
                         value = value.replace(potentialCells[i][0], window.$submited[j].value);
                         swapped = true;
                     }
                 }
                 if(!swapped){
                    value = value.replace(potentialCells[i][0], 0);
                 }
                 swapped = false;
            }
        }
        try {
            eval(value); 
        } catch (e) {
            this.setState((prevValue) =>  {
                return {cellValue : "#error"};
            });
            return;
        }
        this.setState((prevValue) =>  {
            var overWritten = false;
            for (var i = 0; i < window.$submited.length; i++){
                if(window.$submited[i].id == id){
                    window.$submited[i]={
                        id: id,
                        value: eval(value)
                    }
                overWritten = true;
                break;
                }
            }
            if(!overWritten){
            window.$submited.push({
                id: id,
                value: eval(value)
            });
            }
            return {cellValue : eval(value)}
        });
    }

    onCellChange = (oEvent) => {
        this.setState((prevValue) =>  {
            return {cellValue : oEvent.target.value}
        });
    }

    render(){
        var id = this.props.y + this.props.x;
        var self = this;
        return(
            <td>
            <form onSubmit={this.onCellSubmit}>
                <input type="text" onChange={this.onCellChange} onBlur={this.onCellSubmit} className="cell" value={this.state.cellValue} id={id} ref={this.myRef} key={id}></input>
            </form>
            </td>
        )
    }
}

export default Cell;